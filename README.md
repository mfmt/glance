# Glance

glance is a dead simple php script that queries an elasticsearch repo and
reports whether we have any critical, warning or info alerts. It also reports
if any servers have not sent a [pigeon](https://gitlab.com/mfmt/pigeon) alert in the last 10
minutes.

glance can also output in json format if you append `?format=json` to the URL.
