<?php

class ElasticSearchGlance {
  public $username = NULL;
  public $password = NULL;
  public $url = NULL;

  private $one_hour_ago = NULL;
  private $ten_minutes_ago = NULL;
  private $now = NULL;

  private $pigeon_query_template = '{
    "query": {
      "bool": {
        "filter": [
          { 
            "range": {
              "@timestamp": {
                "gte": "TIMESTAMP_GTE",
                "lt": "TIMESTAMP_LTE"
              }
            }
          },
          {
            "term": {
              "systemd.unit": "pigeon.service"  
            }
          },
          {
            "term": {
              "dissect.pigeon_priority": "PIGEON_PRIORITY"
            }
          }
        ]
      }
    }
  }'; 

  private $simplemonitor_query_template = '{
    "query": {
      "bool": {
        "filter": [
          { 
            "range": {
              "@timestamp": {
                "gte": "TIMESTAMP_GTE",
                "lt": "TIMESTAMP_LTE"
              }
            }
          },
          {
            "term": {
              "systemd.unit": "simplemonitor.service"  
            }
          },
          {
            "match": {
              "message": "ERROR"
            }
          }
        ]
      }
    }
  }'; 

  private $pigeon_heartbeat_query_template = '{
"aggs": { 
      "hostname": { 
        "terms": { 
          "field": "host.hostname", 
          "order": { 
            "_count": "desc" 
          }, 
          "size": 1000 
        }, 
        "aggs": { 
          "hostname_count": { 
            "filters": { 
              "filters": {
                "Heartbeats": {
                  "term": {
                    "dissect.pigeon_priority": "heartbeat" 
                  }
                }
              }
            } 
          } 
        } 
      }
    }, 
    "size": 0, 
    "query": { 
      "bool": { 
        "filter": [ 
          { 
            "range": { 
              "@timestamp": { 
                "gte": "now-10m", 
                "lte": "now", 
                "format": "strict_date_optional_time" 
              } 
            } 
          } 
        ] 
      } 
    } 
  }';

  private $format = 'html';

  private $output = [];

  function init() {
    $one_hour = new DateInterval("PT1H");
    $ten_minutes = new DateInterval("PT10M");

    # Setup our times.
    $tz = new DateTimeZone('UTC');
    $this->now = new DateTime('now', $tz);
    $this->one_hour_ago = new DateTime('now', $tz);
    $this->one_hour_ago->sub($one_hour);
    $this->ten_minutes_ago = new DateTime('now', $tz);
    $this->ten_minutes_ago->sub($ten_minutes);

    # How should we format the output?
    if (array_key_exists('format', $_GET)) {
      $this->format = $_GET['format'];
    }
  }

  // $cmd should be either _search or _count.
  function get_curl($cmd) {
    $ch = curl_init($this->url . $cmd);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    return $ch;
  }

  function output() {
    if ($this->format == 'html') {
      $html_template = file_get_contents('index.html.template');
      $find = [
        'SIMPLEMONITOR_CRITICAL_ONE_HOUR',
        'SIMPLEMONITOR_CRITICAL_TEN_MINUTES',
        'PIGEON_CRITICAL_ONE_HOUR',
        'PIGEON_CRITICAL_TEN_MINUTES',
        'PIGEON_WARNING_ONE_HOUR',
        'PIGEON_WARNING_TEN_MINUTES',
        'PIGEON_INFO_ONE_HOUR',
        'PIGEON_INFO_TEN_MINUTES',
        'PIGEON_MISSING_ONE_HOUR',
        'PIGEON_MISSING_TEN_MINUTES',
        'QUERY_TIME'
      ];
      $replace = [
        $this->output['simplemonitor']['critical']['one_hour'],
        $this->output['simplemonitor']['critical']['ten_minutes'],
        $this->output['pigeon']['critical']['one_hour'],
        $this->output['pigeon']['critical']['ten_minutes'],
        $this->output['pigeon']['warning']['one_hour'],
        $this->output['pigeon']['warning']['ten_minutes'],
        $this->output['pigeon']['info']['one_hour'],
        $this->output['pigeon']['info']['ten_minutes'],
        $this->output['pigeon']['missing']['one_hour'],
        $this->output['pigeon']['missing']['ten_minutes'],
        $this->now->format('Y-m-d H:i:s'),
      ];
      echo str_replace($find, $replace, $html_template);
    }
    else {
      echo json_encode($this->output);
    }
  }


  function run() {
    $this->set_content();
    $this->output();
  }

  function set_content() {
    $this->set_priority_content('critical', 'one_hour', 'simplemonitor');
    $this->set_priority_content('critical', 'ten_minutes', 'simplemonitor');

    $this->set_priority_content('critical', 'one_hour', 'pigeon');
    $this->set_priority_content('critical', 'ten_minutes', 'pigeon');

    $this->set_priority_content('warning', 'one_hour', 'pigeon');
    $this->set_priority_content('warning', 'ten_minutes', 'pigeon');

    $this->set_priority_content('info', 'one_hour', 'pigeon');
    $this->set_priority_content('info', 'ten_minutes', 'pigeon');

    $this->set_pigeon_heartbeat_content('one_hour');
    $this->set_pigeon_heartbeat_content('ten_minutes');
  }

  function set_pigeon_heartbeat_content($since) {
    $find = [
      'TIMESTAMP_GTE',
      'TIMESTAMP_LTE',
    ]; 

    $replace_since = NULL;
    if ($since == 'one_hour') {
      $replace_since = $this->one_hour_ago->format('c');
    }
    elseif ($since == 'ten_minutes') {
      $replace_since = $this->ten_minutes_ago->format('c');
    }

    $replace = [
      $replace_since,
      $this->now->format('c'),
    ];
    $payload = str_replace($find, $replace, $this->pigeon_heartbeat_query_template);
    $cmd = '_search';
    $ch = $this->get_curl($cmd);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($payload))
    );
    $raw_response = curl_exec($ch);
    if (curl_errno($ch)) {
        $error_msg = curl_error($ch);
    }
    curl_close($ch);

    if (isset($error_msg)) {
        // TODO - Handle cURL error accordingly
      echo "Error: " . $error_msg . "\n";
      echo "Error: " . $raw_response . "\n";
    }

    $output = json_decode($raw_response, true);

    $missing = [];
    foreach ($output['aggregations']['hostname']['buckets'] as $bucket) {
      if (intval($bucket['hostname_count']['buckets']['Heartbeats']['doc_count']) == 0) {
        $missing[] = $bucket['key'];
      }
    }
    $this->output['pigeon']['missing'][$since] = implode(',', $missing);
  }

  function set_priority_content($priority, $since, $service) {
    $find = [
      'TIMESTAMP_GTE',
      'TIMESTAMP_LTE',
      'PIGEON_PRIORITY',
    ]; 

    $replace_since = NULL;
    if ($since == 'one_hour') {
      $replace_since = $this->one_hour_ago->format('c');
    }
    elseif ($since == 'ten_minutes') {
      $replace_since = $this->ten_minutes_ago->format('c');
    }

    $replace = [
      $replace_since,
      $this->now->format('c'),
      $priority, 
    ];

    if ($service == 'pigeon') {
      $template = $this->pigeon_query_template;
    }
    else {
      $template = $this->simplemonitor_query_template;
    }

    $payload = str_replace($find, $replace, $template);
    $cmd = '_count';
    $ch = $this->get_curl($cmd);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($payload))
    );
    $raw_response = curl_exec($ch);
    if (curl_errno($ch)) {
        $error_msg = curl_error($ch);
    }
    curl_close($ch);

    if (isset($error_msg)) {
        // TODO - Handle cURL error accordingly
      echo "Error: " . $error_msg . "\n";
      echo "Error: " . $raw_response . "\n";
    }

    $output = json_decode($raw_response, true);

    $this->output[$service][$priority][$since] = $output['count'];
  }
  
}

$glance = new ElasticSearchGlance();

include('config.php');

$glance->username = $username;
$glance->password = $password;
$glance->url = $url;

$glance->init();
$glance->run();


